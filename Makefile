# $Id: Makefile 19 2017-05-12 21:12:00Z umaxx $
# Copyright (c) 2011-2017 Joerg Jung <jung@openbsd.org>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

CC?=gcc
INSTALL?=install
RM?=rm -f

PLATFORM!=uname
PLATFORM?=$(shell uname)
MACHINE?=$(shell uname -m)

PREFIX_Darwin=/usr
PREFIX_Linux=/usr
PREFIX_OpenBSD=/usr/local
PREFIX?=$(PREFIX_$(PLATFORM))

BINDIR?=$(PREFIX)/bin
INCDIR?=$(PREFIX)/include
LIBDIR_i386=$(PREFIX)/lib
LIBDIR_i686=$(PREFIX)/lib
LIBDIR_amd64=$(PREFIX)/lib
LIBDIR_x86_64=$(PREFIX)/lib64
LIBDIR?=$(LIBDIR_$(MACHINE))
MANDIR_Darwin=$(PREFIX)/share/man
MANDIR_Linux=$(PREFIX)/share/man
MANDIR_OpenBSD=$(PREFIX)/man
MANDIR?=$(MANDIR_$(PLATFORM))

CFLAGS?=-Os
CFLAGS+=-ansi -pedantic -Wall -Wextra
CFLAGS+=-Isrc -I/usr/include -I$(INCDIR) -I/usr/X11R6/include
CFLAGS_Darwin=
CFLAGS_Linux=-fPIC -D_BSD_SOURCE -D_XOPEN_SOURCE=600 -D_GNU_SOURCE
CFLAGS_OpenBSD=
CFLAGS+=$(CFLAGS_$(PLATFORM))

LDFLAGS+=-L$(LIBDIR) -L/usr/lib -L/usr/X11R6/lib

LIBS+=-lX11 -lXrandr
LIBS_Darwin=
LIBS_Linux=-lrt
LIBS_OpenBSD=
LIBS+=$(LIBS_$(PLATFORM))

OBJECTS=sct.o

all: sct

.c.o:
	$(CC) -c $(CFLAGS) -o $@ $<

sct: $(OBJECTS)
	$(CC) $(LDFLAGS) -o sct $(OBJECTS) $(LIBS)

clean:
	$(RM) $(OBJECTS) sct sct.core

install: sct
	$(INSTALL) -m0755 sct $(BINDIR)
	$(INSTALL) -m0755 sctd.sh $(BINDIR)/sctd
	$(INSTALL) -m0444 sct.1 $(MANDIR)/man1
	$(INSTALL) -m0444 sctd.1 $(MANDIR)/man1

uninstall:
	$(RM) $(BINDIR)/sct $(MANDIR)/man1/sct.1
	$(RM) $(BINDIR)/sctd $(MANDIR)/man1/sctd.1

.PHONY: all clean install uninstall
